const readdir = require("recursive-readdir");
const loadJsonFile = require('load-json-file');
const writeJsonFile = require('write-json-file');
const log = require('npmlog');
const dayjs = require('dayjs');
function escapeRegExp(string) {
    return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
}
async function search(keyword) {
    log.info('init', 'get all file path in data');
    let filePaths = await readdir("data");
    let escapeKeyword = escapeRegExp(keyword);
    let keywordRegex = new RegExp(escapeKeyword, 'ig')
    let searchJob = filePaths.map(async filePath => {
        let json = await loadJsonFile(filePath);
        let isInContext = keywordRegex.test(json.content)
        let inMessages = json.messages.filter(e =>
            keywordRegex.test(e.push_content)
        );
        let date = dayjs(json.date).toDate().toLocaleString();
        let title = json.article_title
        let url =`https://www.ptt.cc/bbs/${json.board}/${json.article_id}.html`
        return {
            title,
            date,
            url,
            isInContext,
            inMessages
        };
    })
    let result = await Promise.all(searchJob);
    let filterResult = result.filter(e=>e.isInContext||e.inMessages.length).sort((a,b)=>a.date-b.date);
    await writeJsonFile(`result/${keyword}-${dayjs().format('YYYY-MM-DD HH-mm')}.json`, filterResult);
}

(async () => {
    await search('');
})()